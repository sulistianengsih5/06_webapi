﻿using Microsoft.AspNetCore.Mvc;

namespace _06_WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        [HttpGet (Name = "GetEmployees")]
        public IEnumerable<Employee> Get()
        {
            return GetEmployees();
        }

        [HttpGet("{id}")]
        public Employee Find(int id)
        {
            return GetEmployees().Find(e => e.Id == id);
        }

        [HttpPost]
        [Produces("application/json")]
        public Employee Post([FromBody] Employee employee)
        {
            return new Employee();
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Employee employee)
        {
            // Logic to update an Employee
        }


        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }

        private List<Employee> GetEmployees()
        {
            return new List<Employee>()
            {
                new Employee()
                {
                    Id = 1,
                    FirstName= "John",
                    LastName = "Smith",
                    EmailId ="John.Smith@gmail.com"
                },
                new Employee()
                {
                    Id = 2,
                    FirstName= "Jane",
                    LastName = "Doe",
                    EmailId ="Jane.Doe@gmail.com"
                }
            };
        }
    }
}
