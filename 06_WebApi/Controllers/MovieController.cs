﻿using _06_WebApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace _06_WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieController : ControllerBase
    {
        [HttpPost (Name ="create")]
        public Movie Create(Movie movie)
        {
            movie.Id = MovieRepository.Movies.Count + 1;
            MovieRepository.Movies.Add(movie);

            return movie;
        }

        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            var oldMovie = MovieRepository.Movies.FirstOrDefault(x => x.Id == id);

            if (oldMovie is null) return false;

            MovieRepository.Movies.Remove(oldMovie);

            return true;
        }

        [HttpGet("{id}")]
        public Movie Get(int id)
        {
            var movie = MovieRepository.Movies.FirstOrDefault(x => x.Id == id);

            if (movie is null)
            {
                return null;
            }

            return movie;
        }

        [HttpGet(Name = "GetMovies")]
        public IEnumerable<Movie> List()
        {
            var movies = MovieRepository.Movies;

            return movies;
        }

        [HttpPut("{id}")]
        public Movie Update(Movie newMovie)
        {
            var oldMovie = MovieRepository.Movies.FirstOrDefault(o => o.Id == newMovie.Id);

            if (oldMovie is null) return null;

            oldMovie.Title = newMovie.Title;
            oldMovie.Description = newMovie.Description;
            oldMovie.Rating = newMovie.Rating;

            return newMovie;
        }

    }
}
