﻿using Microsoft.AspNetCore.Mvc;

namespace _06_WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        [HttpGet (Name = "GetUsers")]
        public IEnumerable<User> Get()
        {
            return GetUsers();
        }

        [HttpGet("{id}", Name ="Get")]
        public User Get(int id)
        {
            return GetUsers().Find(e => e.Id == id);
        }

        /*
        [HttpPost]
        public bool Login(string username, string password)
        {
            return false;
        }
        */

        private List<User> GetUsers()
        {
            return new List<User>()
            {
                new User()
                {
                    Username = "Sulistia",
                    Password = "password"
                },
                new User()
                {
                    Username = "Sulistia2",
                    Password = "password"
                }
            };
        }

    }
}
