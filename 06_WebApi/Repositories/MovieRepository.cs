﻿namespace _06_WebApi.Repositories
{
    public class MovieRepository
    {
        public static List<Movie> Movies = new()
        {
            new()
            {
                Id = 1,
                Title = "Look Like an Angel",
                Description = "Talk like an angel, move like an angel.",
                Rating = "4.0"
            },
            new()
            {
                Id = 2,
                Title = "Petty",
                Description = "A mediocore movie.",
                Rating = "6.1"
            },
            new()
            {
                Id = 3,
                Title = "Boys Before Flowers",
                Description = "You'll need a constant reminder that this K-Drama isn't potraying the real love life of an 18 years old highschool girl.",
                Rating = "6.6"
            },
        };
    }
}
