﻿namespace _06_WebApi.Repositories
{
    public class UserRepository
    {
        public static List<User> Users = new()
        {
            new()
            {
                Username = "Sulistia",
                EmailAddress = "sulistia.nengsih@suitmedia.com",
                Password = "Password",
                GivenName = "Sulistia",
                Surname = "Nengsih",
                Role = "Administrator"
            },
            new()
            {
                Username = "Abdul",
                EmailAddress = "abdulazim0513@gmail.com",
                Password = "Password",
                GivenName = "Abdul",
                Surname = "Azim",
                Role = "Standard"
            },
        };
    }
}
